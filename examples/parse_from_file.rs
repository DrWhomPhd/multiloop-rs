use std::env;
use std::fs::File;
use std::io::{BufReader, BufRead, Read};

pub fn main() {
    let file_name: Vec<String> = env::args().collect();
    let file_name = file_name[1].to_owned();

    println!("Opening {}...", file_name);
    let mut buffer = String::new();
    BufReader::new(File::open(file_name).expect("Could not open file.")).read_to_string(&mut buffer);
    
    for line in buffer.lines() {
        // Some lines may contain \r\n still and should be further split, clean up whitespace prior to split
        let split_buffer = line.trim_end().split("\r\n");

        // Loop through the final split buffer
        for line in split_buffer {
            let packet = multiloop_rs::decode_packet(line).expect(format!("Could not decode: {}", line).as_str());
        }
    }
}
