# multiloop-rs

This project provides a library, written in rust, for decoding and encoding multiloop 
timing packets. Multiloop is a timing protocol used in motorsport. It is purposefully
connection agnostic to allow for the utmost flexibility for library usage (i.e., wasm
deployment vs. async network deployments vs. no_std embedded systems).

## Why Write multiloop-rs?

The end goal of this work is to enable fans of GT and other racing to track the
narratives in races that cannot be done via the usual information provided in
live timing graphics provided in streaming or televised broadcasts.

Most information provided to fans of motorsport is limited to lap times and
interval to the next car. Rarely do they provide information such as pit stops,
lap deltas, chasing predictions, and other statistics that allow  viewer to
have an in-depth understanding of what's going on in a race.

## Multiloop Protocol Semantics (Best Guess)

The following documentation is my best understanding of multiloop messages based on
the processing of real time race messages.

### Heartbeats 
* laps_to_go is used when the timer hits 0 but the race continues for a final lap.

### Determining race starts and endings
* `heartbeats_sent` resets to 0 when a race is about to start. `TrackStatus` changes 
from `TrackStatus::C` to `TrackStatus::U`.
* EntryInformation will be sent for all drivers when `TrackStatus::C` changes to 
`TrackStatus::U`
* Heartbeat's time_to_go will change from 0 to the duration of the race when 
`TrackStatus::C` changes to `TrackStatus::U`

## References

[1] https://help.hhtiming.com/article/158-sro-america