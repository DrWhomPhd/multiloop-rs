// Copyright (C) 2023- Nathaniel Husted
//
// This file is part of multiloop-rs.
//
// multiloop-rs is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// multiloop-rs is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with multiloop-rs. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>

use core::panic;
use std::any::Any;
use std::boxed::Box;
use std::fmt;
use std::fmt::Formatter;

#[derive(Debug, PartialEq)]
pub enum PacketType {
    H,      // Heartbeat, race status, time and time of day information.
    E,      // Current entry information.
    C,      // Completed lap results.
    S,      // Section information.
    L,      // Line crossing message.
    F,      // Flag information.
    I,      // Invalidated lap information.
    N,      // New leader.
    R,      // Current run information.
    T,      // Current track / section information.
    A,      // Announcement information.
    V,      // Version stream information.
    USACTZ, // Found in the GT America Multiloop Feed
    USACS,  // Found in the GT America Multiloop Feed
    USACDR, // USAC:DRIVERID; Found in the GT America Multiloop Feed
}

#[derive(Debug, PartialEq)]
pub enum TrackStatus {
    G, // Green
    Y, // Yellow
    R, // Red
    W, // White
    K, // Checkered
    U, // Unflagged -- Warmup/Pace Laps
    C, // Cold -- Protocol species it's only used in Heartbeat Packets
}

#[derive(Debug, PartialEq)]
pub enum RecordType {
    N, // New
    R, // Repeated
    U, // Updated
}

#[derive(Debug, PartialEq)]
pub enum LapStatus {
    T, // Track
    P, // Pitlane
}

#[derive(Debug, PartialEq)]
pub enum LineCrossingSource {
    A, // Antenna
    M, // Manual
    P, // Photocell
}

#[derive(Debug, PartialEq)]
pub enum RunType {
    P, // Practice
    Q, // Qualifying
    S, // Single Car Qualifying
    R, // Race
}

/******************************************************
// Protocol Message structs
//
// NOTE:
// Allegedly the unique ID number is supposed to max at
// 0xFFFFF and restart at 0 but the sample file (from
// USAC Nation) doesn't appear to abide by this part of
// the specification.
 */
pub trait Multiloop {
    fn get_type(&self) -> PacketType;
    fn as_any(&self) -> &dyn Any;
    fn get_record_type(&self) -> &RecordType;
}

#[derive(Debug)]
pub struct Heartbeat {
    pub record_type: RecordType,
    pub heartbeats_sent: u32, // heartbeats sent -- UID
    pub track_status: TrackStatus,
    pub time_date: u32,    // Time in seconds since 1/1/70
    pub elapsed_time: u32, // Time in milliseconds (hex chars).
    pub laps_to_go: u16,
    pub time_to_go: u32, // Time in milliseconds (hex chars)
}

impl Heartbeat {
    fn new(header: Vec<&str>, body: Vec<&str>) -> Heartbeat {
        Heartbeat {
            record_type: parse_result_type(header[1]).unwrap(),
            heartbeats_sent: u32::from_str_radix(header[2], 16).unwrap(),
            track_status: match body[0] {
                "G" => Ok(TrackStatus::G),
                "Y" => Ok(TrackStatus::Y),
                "R" => Ok(TrackStatus::R),
                "W" => Ok(TrackStatus::W),
                "K" => Ok(TrackStatus::K),
                "U" => Ok(TrackStatus::U),
                "C" => Ok(TrackStatus::C),
                _ => Err(DecodeError),
            }
            .unwrap(),
            time_date: u32::from_str_radix(body[1], 16).unwrap(),
            elapsed_time: u32::from_str_radix(body[2], 16).unwrap(),
            laps_to_go: u16::from_str_radix(body[3], 16).unwrap(),
            time_to_go: u32::from_str_radix(body[4], 16).unwrap(),
        }
    }
    fn new_multiloop(header: Vec<&str>, body: Vec<&str>) -> Box<dyn Multiloop> {
        Box::new(Heartbeat::new(header, body))
    }
}
impl Multiloop for Heartbeat {
    fn get_type(&self) -> PacketType {
        PacketType::H
    }
    fn as_any(&self) -> &dyn Any {
        self
    }
    fn get_record_type(&self) -> &RecordType {
        &self.record_type
    }
}

#[derive(Debug)]
pub struct CompletedLap {
    pub record_type: RecordType,
    pub result_id: u16,
    pub lap_number: u16,
    pub rank: u16,
    pub number: String,
    pub uid: u16,
    pub completed_laps: u16,
    pub elapsed_time: u32,
    pub last_laptime: u32,
    pub lap_status: LapStatus,
    pub fastest_laptime: u32,
    pub fastest_lap: u16,
    pub time_behind_leader: u32,
    pub laps_behind_leader: u16,
    pub time_behind_prec: u32,
    pub laps_behind_prec: u16,
    pub overall_rank: u16,
    pub overall_best_laptime: u32,
    pub current_status: String,
    pub track_status: Option<TrackStatus>,
    pub pit_stop_count: u16,
    pub last_pitted_lap: u16,
    pub start_position: u16,
    pub laps_led: u16,
}

impl CompletedLap {
    fn new(header: Vec<&str>, body: Vec<&str>) -> CompletedLap {
        let (result_id, lap_number) = parse_variable_uniqueid(header[2]);
        CompletedLap {
            record_type: parse_result_type(header[1]).unwrap(),
            result_id,
            lap_number,
            rank: u16::from_str_radix(body[0], 16).unwrap(),
            number: body[1].to_string(),
            uid: u16::from_str_radix(body[2], 16).unwrap(),
            completed_laps: u16::from_str_radix(body[3], 16).unwrap(),
            elapsed_time: u32::from_str_radix(body[4], 16).unwrap(),
            last_laptime: u32::from_str_radix(body[5], 16).unwrap(),
            lap_status: match body[6] {
                "T" => LapStatus::T,
                "P" => LapStatus::P,
                _ => panic!("Error decoding lap status"),
            },
            fastest_laptime: u32::from_str_radix(body[7], 16).unwrap(),
            fastest_lap: u16::from_str_radix(body[8], 16).unwrap(),
            time_behind_leader: u32::from_str_radix(body[9], 16).unwrap(),
            laps_behind_leader: u16::from_str_radix(body[10], 16).unwrap(),
            time_behind_prec: u32::from_str_radix(body[11], 16).unwrap(),
            laps_behind_prec: u16::from_str_radix(body[12], 16).unwrap(),
            overall_rank: u16::from_str_radix(body[13], 16).unwrap(),
            overall_best_laptime: u32::from_str_radix(body[14], 16).unwrap(),
            current_status: body[15].to_string(),
            track_status: match body[16] {
                "G" => Some(TrackStatus::G),
                "Y" => Some(TrackStatus::Y),
                "R" => Some(TrackStatus::R),
                "W" => Some(TrackStatus::W),
                "K" => Some(TrackStatus::K),
                "U" => Some(TrackStatus::U),
                _ => None,
            },
            pit_stop_count: u16::from_str_radix(body[17], 16).unwrap(),
            last_pitted_lap: u16::from_str_radix(body[18], 16).unwrap(),
            start_position: u16::from_str_radix(body[19], 16).unwrap(),
            laps_led: u16::from_str_radix(body[20], 16).unwrap(),
        }
    }
    fn new_multiloop(header: Vec<&str>, body: Vec<&str>) -> Box<dyn Multiloop> {
        Box::new(CompletedLap::new(header, body))
    }
}
impl Multiloop for CompletedLap {
    fn get_type(&self) -> PacketType {
        PacketType::C
    }

    fn as_any(&self) -> &dyn Any {
        self
    }
    fn get_record_type(&self) -> &RecordType {
        &self.record_type
    }
}

#[derive(Debug)]
pub struct CompletedSection {
    pub record_type: RecordType,
    pub result_id: u16,
    pub sector_id: u16,
    pub number: String,
    pub uid: u16,
    pub section_name: String,
    pub elapsed_time: u32,
    pub last_section_time: u32,
    pub last_lap: u16,
}
impl CompletedSection {
    fn new(header: Vec<&str>, body: Vec<&str>) -> CompletedSection {
        let (result_id, sector_id) = parse_variable_uniqueid(header[2]);
        CompletedSection {
            record_type: parse_result_type(header[1]).unwrap(),
            result_id,
            sector_id,
            number: body[0].to_string(),
            uid: if body[1].len() > 4 {
                u16::from_str_radix(body[1][body[1].len() - 4..body[1].len()].as_ref(), 16).unwrap()
            } else {
                u16::from_str_radix(body[1], 16).unwrap()
            },
            section_name: body[2].to_string(),
            elapsed_time: u32::from_str_radix(body[3], 16).unwrap(),
            last_section_time: u32::from_str_radix(body[4], 16).unwrap(),
            last_lap: u16::from_str_radix(body[5], 16).unwrap(),
        }
    }
    fn new_multiloop(header: Vec<&str>, body: Vec<&str>) -> Box<dyn Multiloop> {
        Box::new(CompletedSection::new(header, body))
    }
}
impl Multiloop for CompletedSection {
    fn get_type(&self) -> PacketType {
        PacketType::S
    }

    fn as_any(&self) -> &dyn Any {
        self
    }
    fn get_record_type(&self) -> &RecordType {
        &self.record_type
    }
}

#[derive(Debug)]
pub struct LineCrossing {
    pub record_type: RecordType,
    pub passing_id: u32,
    pub number: String,
    pub uid: u16,
    pub time_line: String,
    pub source: LineCrossingSource,
    pub elapsed_time: u32,
    pub track_status: TrackStatus,
    pub crossing_status: LapStatus,
}
impl LineCrossing {
    fn new(header: Vec<&str>, body: Vec<&str>) -> LineCrossing {
        LineCrossing {
            record_type: parse_result_type(header[1]).unwrap(),
            passing_id: u32::from_str_radix(header[2], 16).unwrap(),
            number: body[0].to_string(),
            uid: u16::from_str_radix(body[1], 16).unwrap(),
            time_line: body[2].to_string(),
            source: match body[3] {
                "A" => LineCrossingSource::A,
                "M" => LineCrossingSource::M,
                "P" => LineCrossingSource::P,
                _ => panic!("Invlid Line Crossing Source: {}", body[3]),
            },
            elapsed_time: u32::from_str_radix(body[4], 16).unwrap(),
            track_status: match body[5] {
                "C" => TrackStatus::C,
                "G" => TrackStatus::G,
                "K" => TrackStatus::K,
                "R" => TrackStatus::R,
                "U" => TrackStatus::U,
                "W" => TrackStatus::W,
                "Y" => TrackStatus::Y,
                _ => panic!("Invalid Track Status: {}", body[6]),
            },
            crossing_status: match body[6] {
                "P" => LapStatus::P,
                "T" => LapStatus::T,
                _ => panic!("Invalid Crossing Status {}", body[6]),
            },
        }
    }
    fn new_multiloop(header: Vec<&str>, body: Vec<&str>) -> Box<dyn Multiloop> {
        Box::new(LineCrossing::new(header, body))
    }
}
impl Multiloop for LineCrossing {
    fn get_type(&self) -> PacketType {
        PacketType::L
    }

    fn as_any(&self) -> &dyn Any {
        self
    }
    fn get_record_type(&self) -> &RecordType {
        &self.record_type
    }
}

#[derive(Debug)]
pub struct InvalidatedLap {
    pub record_type: RecordType,
    pub passing_id: u32,
    pub number: String,
    pub uid: u32, // Real world packets have this the same value as passing_id
    pub elapsed_time: u32,
}

impl InvalidatedLap {
    fn new(header: Vec<&str>, body: Vec<&str>) -> InvalidatedLap {
        InvalidatedLap {
            record_type: parse_result_type(header[1]).unwrap(),
            passing_id: u32::from_str_radix(header[2], 16).unwrap(),
            number: body[0].to_string(),
            uid: u32::from_str_radix(body[1], 16).unwrap(),
            elapsed_time: u32::from_str_radix(body[2], 16).unwrap(),
        }
    }
    fn new_multiloop(header: Vec<&str>, body: Vec<&str>) -> Box<dyn Multiloop> {
        Box::new(InvalidatedLap::new(header, body))
    }
}
impl Multiloop for InvalidatedLap {
    fn get_type(&self) -> PacketType {
        PacketType::I
    }

    fn as_any(&self) -> &dyn Any {
        self
    }
    fn get_record_type(&self) -> &RecordType {
        &self.record_type
    }
}

#[derive(Debug)]
pub struct FlagInformation {
    pub record_type: RecordType,
    pub flag_changes: u32,
    pub track_status: TrackStatus,
    pub lap_number: u16,
    pub green_time: u32,
    pub green_laps: u16,
    pub yellow_time: u32,
    pub yellow_laps: u16,
    pub red_time: u32,
    pub num_yellows: u16,
    pub current_leader: String,
    pub num_lead_changes: u16,
    pub avg_race_speed: String, // std. says time/distance. Presume mph in US kph elsewhere? Std unclear.
}
impl FlagInformation {
    fn new(header: Vec<&str>, body: Vec<&str>) -> FlagInformation {
        FlagInformation {
            record_type: parse_result_type(header[1]).unwrap(),
            flag_changes: u32::from_str_radix(header[2], 16).unwrap(),
            track_status: match body[0] {
                "C" => TrackStatus::C,
                "G" => TrackStatus::G,
                "K" => TrackStatus::K,
                "R" => TrackStatus::R,
                "U" => TrackStatus::U,
                "W" => TrackStatus::W,
                "Y" => TrackStatus::Y,
                _ => panic!("Invalid Track Status Type"),
            },
            lap_number: u16::from_str_radix(body[1], 16).unwrap(),
            green_time: u32::from_str_radix(body[2], 16).unwrap(),
            green_laps: u16::from_str_radix(body[3], 16).unwrap(),
            yellow_time: u32::from_str_radix(body[4], 16).unwrap(),
            yellow_laps: u16::from_str_radix(body[5], 16).unwrap(),
            red_time: u32::from_str_radix(body[6], 16).unwrap(),
            num_yellows: u16::from_str_radix(body[7], 16).unwrap(),
            current_leader: body[8].to_string(),
            num_lead_changes: u16::from_str_radix(body[9], 16).unwrap(),
            avg_race_speed: body[10].to_string(),
        }
    }
    fn new_multiloop(header: Vec<&str>, body: Vec<&str>) -> Box<dyn Multiloop> {
        Box::new(FlagInformation::new(header, body))
    }
}
impl Multiloop for FlagInformation {
    fn get_type(&self) -> PacketType {
        PacketType::F
    }

    fn as_any(&self) -> &dyn Any {
        self
    }
    fn get_record_type(&self) -> &RecordType {
        &self.record_type
    }
}

#[derive(Debug)]
pub struct NewLeaderInformation {
    pub record_type: RecordType,
    pub num_lead_changes: u32,
    pub new_leader: String,
    pub uid: u16,
    pub lap_number: u16,
    pub elapsed_time: u32,
    pub lead_change_index: u16,
}
impl NewLeaderInformation {
    fn new(header: Vec<&str>, body: Vec<&str>) -> NewLeaderInformation {
        NewLeaderInformation {
            record_type: parse_result_type(header[1]).unwrap(),
            num_lead_changes: u32::from_str_radix(header[2], 16).unwrap(),
            new_leader: body[0].to_string(),
            uid: u16::from_str_radix(body[1], 16).unwrap(),
            lap_number: u16::from_str_radix(body[2], 16).unwrap(),
            elapsed_time: u32::from_str_radix(body[3], 16).unwrap(),
            lead_change_index: u16::from_str_radix(body[4], 16).unwrap(),
        }
    }
    fn new_multiloop(header: Vec<&str>, body: Vec<&str>) -> Box<dyn Multiloop> {
        Box::new(NewLeaderInformation::new(header, body))
    }
}
impl Multiloop for NewLeaderInformation {
    fn get_type(&self) -> PacketType {
        PacketType::N
    }

    fn as_any(&self) -> &dyn Any {
        self
    }
    fn get_record_type(&self) -> &RecordType {
        &self.record_type
    }
}

#[derive(Debug)]
pub struct EntryInformation {
    pub record_type: RecordType,
    pub result_id: u32,
    pub number: String,
    pub uid: u16,
    pub driver_name: String,
    pub start_pos: u8,
    pub info_count: u8, // This should equal the size of the info field.
    pub info: Vec<String>,
    pub competitor_id: u16,
}

impl EntryInformation {
    fn new(header: Vec<&str>, body: Vec<&str>) -> EntryInformation {
        let info_count = u8::from_str_radix(body[4], 16).unwrap();
        EntryInformation {
            record_type: parse_result_type(header[1]).unwrap(),
            result_id: u32::from_str_radix(header[2], 16).unwrap(),
            number: body[0].to_string(),
            uid: u16::from_str_radix(body[1], 16).unwrap(),
            driver_name: body[2].to_string(),
            start_pos: u8::from_str_radix(body[3], 16).unwrap(),
            info_count: info_count,
            info: body[5..5 + info_count as usize]
                .to_vec()
                .into_iter()
                .map(|i| i.to_string())
                .collect(),
            competitor_id: u16::from_str_radix(body[5 + info_count as usize], 16).unwrap(),
        }
    }
    fn new_multiloop(header: Vec<&str>, body: Vec<&str>) -> Box<dyn Multiloop> {
        Box::new(EntryInformation::new(header, body))
    }
}
impl Multiloop for EntryInformation {
    fn get_type(&self) -> PacketType {
        PacketType::E
    }

    fn as_any(&self) -> &dyn Any {
        self
    }
    fn get_record_type(&self) -> &RecordType {
        &self.record_type
    }
}

#[derive(Debug)]
pub struct RunInformation {
    pub record_type: RecordType,
    pub run_id: u32,
    pub event_name: String,
    pub short_name: String,
    pub run_name: String,
    pub run_type: RunType,
    pub start_datetime: u32, // Time in seconds since 1970
}

impl RunInformation {
    fn new(header: Vec<&str>, body: Vec<&str>) -> RunInformation {
        RunInformation {
            record_type: parse_result_type(header[1]).unwrap(),
            run_id: u32::from_str_radix(header[2], 16).unwrap(),
            event_name: body[0].to_string(),
            short_name: body[1].to_string(),
            run_name: body[2].to_string(),
            run_type: match body[3] {
                "P" => RunType::P,
                "Q" => RunType::Q,
                "R" => RunType::R,
                "S" => RunType::S,
                _ => panic!("Unknown RunType: {}", body[3]),
            },
            start_datetime: u32::from_str_radix(body[4], 16).unwrap(),
        }
    }
    fn new_multiloop(header: Vec<&str>, body: Vec<&str>) -> Box<dyn Multiloop> {
        Box::new(RunInformation::new(header, body))
    }
}
impl Multiloop for RunInformation {
    fn get_type(&self) -> PacketType {
        PacketType::R
    }

    fn as_any(&self) -> &dyn Any {
        self
    }
    fn get_record_type(&self) -> &RecordType {
        &self.record_type
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct SectionInformation {
    pub section_name: String,
    pub section_length: String, // Length in whole inches
    pub section_start_lbl: String,
    pub section_end_lbl: String,
}
#[derive(Debug)]
pub struct TrackInformation {
    pub record_type: RecordType,
    pub track_id: u32,
    pub track_name: String,
    pub venue: String,
    pub track_length: String, // Length in Miles
    pub num_sections: u8,
    pub section_info: Vec<SectionInformation>,
}
impl TrackInformation {
    fn new(header: Vec<&str>, body: Vec<&str>) -> TrackInformation {
        let num_sections = u8::from_str_radix(body[3], 16).unwrap();
        let expected_sections = (4 + (num_sections * 4)) as usize;
        if body.len() != expected_sections {
            panic!(
                "Unexpected field count ({} vs. {}) in body: {:?}",
                body.len(),
                expected_sections,
                body
            )
        }
        TrackInformation {
            record_type: parse_result_type(header[1]).unwrap(),
            track_id: u32::from_str_radix(header[2], 16).unwrap(),
            track_name: body[0].to_string(),
            venue: body[1].to_string(),
            track_length: body[2].to_string(),
            num_sections,
            section_info: {
                let mut i = 4;
                let mut output: Vec<SectionInformation> = Vec::new();
                while i < body.len() {
                    output.push(SectionInformation {
                        section_name: body[i].to_string(),
                        section_length: body[i + 1].to_string(),
                        section_start_lbl: body[i + 2].to_string(),
                        section_end_lbl: body[i + 3].to_string(),
                    });
                    // Each section has 4 elements for its information, so we skip to the next section.
                    i += 4;
                }
                output.to_owned()
            },
        }
    }
    fn new_multiloop(header: Vec<&str>, body: Vec<&str>) -> Box<dyn Multiloop> {
        Box::new(TrackInformation::new(header, body))
    }
}
impl Multiloop for TrackInformation {
    fn get_type(&self) -> PacketType {
        PacketType::T
    }

    fn as_any(&self) -> &dyn Any {
        self
    }
    fn get_record_type(&self) -> &RecordType {
        &self.record_type
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum Action {
    A, // Add
    D, // Delete
    M, // Modify
}
#[derive(Debug, Clone, PartialEq)]
pub enum Priority {
    U, // Urgent
    H, // High
    N, // Normal
    L, // Low
}

#[derive(Debug)]
pub struct Announcement {
    pub record_type: RecordType,
    pub announcement_number: u32,
    pub msg_num: u16,
    pub action: Action,
    pub priority: Priority,
    pub timestamp: u32,
    pub test: String,
}
impl Announcement {
    fn new(header: Vec<&str>, body: Vec<&str>) -> Announcement {
        Announcement {
            record_type: parse_result_type(header[1]).unwrap(),
            announcement_number: u32::from_str_radix(header[2], 16).unwrap(),
            msg_num: u16::from_str_radix(body[0], 16).unwrap(),
            action: match body[1] {
                "A" => Action::A,
                "D" => Action::D,
                "M" => Action::M,
                _ => panic!("Unknown action type {}", body[1]),
            },
            priority: match body[2] {
                "H" => Priority::H,
                "L" => Priority::L,
                "N" => Priority::N,
                "U" => Priority::U,
                _ => panic!("Unknown priority type {}", body[2]),
            },
            timestamp: u32::from_str_radix(body[3], 16).unwrap(),
            test: body[4].to_string(),
        }
    }
    fn new_multiloop(header: Vec<&str>, body: Vec<&str>) -> Box<dyn Multiloop> {
        Box::new(Announcement::new(header, body))
    }
}
impl Multiloop for Announcement {
    fn get_type(&self) -> PacketType {
        PacketType::A
    }

    fn as_any(&self) -> &dyn Any {
        self
    }
    fn get_record_type(&self) -> &RecordType {
        &self.record_type
    }
}

#[derive(Debug)]
pub struct Version {
    pub record_type: RecordType,
    pub vid: u8, // Header ID is always 1 per standard.
    pub major: u16,
    pub minor: u16,
    pub info: String,
}
impl Version {
    fn new(header: Vec<&str>, body: Vec<&str>) -> Version {
        Version {
            record_type: parse_result_type(header[1]).unwrap(),
            vid: u8::from_str_radix(header[2], 16).unwrap(),
            // Real world packets show some version packets are missing a body section.
            // No information is available on how to handle these situations so I am
            // assuming we should just 0-out the fields.
            major: u16::from_str_radix(body.get(0).unwrap_or(&""), 16).unwrap_or(0),
            minor: u16::from_str_radix(body.get(1).unwrap_or(&""), 16).unwrap_or(0),
            info: body.get(2).unwrap_or(&"").to_string(),
        }
    }
    fn new_multiloop(header: Vec<&str>, body: Vec<&str>) -> Box<dyn Multiloop> {
        Box::new(Version::new(header, body))
    }
}
impl Multiloop for Version {
    fn get_type(&self) -> PacketType {
        PacketType::V
    }

    fn as_any(&self) -> &dyn Any {
        self
    }
    fn get_record_type(&self) -> &RecordType {
        &self.record_type
    }
}

// Unique packet found in USAC Multiloop implementations.
// The packet has no body
#[derive(Debug)]
pub struct USACTimeZone {
    pub record_type: RecordType,
    pub tz: String,        // Unix-style Timezone string (Best guess)
    pub current_time: u64, // Milliseconds after the UNIX Epoch (Best guess)
}
impl USACTimeZone {
    // The USACTZ Packet doesn't have a body
    fn new(header: Vec<&str>) -> USACTimeZone {
        USACTimeZone {
            record_type: parse_result_type(header[1]).unwrap(),
            tz: header[2].to_string(),
            current_time: u64::from_str_radix(header[3], 16).unwrap(),
        }
    }

    fn new_multiloop(header: Vec<&str>, body: Option<Vec<&str>>) -> Box<dyn Multiloop> {
        Box::new(USACTimeZone::new(header))
    }
}
impl Multiloop for USACTimeZone {
    fn get_type(&self) -> PacketType {
        PacketType::USACTZ
    }

    fn as_any(&self) -> &dyn Any {
        self
    }
    fn get_record_type(&self) -> &RecordType {
        &self.record_type
    }
}

// Unsure what USECS is. I presume S = Sector?
// I'll refactor when I know what S means.
// The packet has no body.
#[derive(Debug)]
pub struct USACS {
    pub record_type: RecordType,
    pub uid: String,       // Unsure what this refers to.Best guess: Car #.
    pub current_time: u64, // Milliseconds after the UNIX Epoch. Best guess.
    pub sector_name: String,
    pub unknown1: u32, // Unsure what this field is. Could be sector time?
}
impl USACS {
    fn new(header: Vec<&str>) -> USACS {
        USACS {
            record_type: parse_result_type(header[1]).unwrap(),
            uid: header[2].to_string(),
            current_time: u64::from_str_radix(header[3], 16).unwrap(),
            sector_name: header[4].to_string(),
            unknown1: u32::from_str_radix(header[5], 16).unwrap(),
        }
    }
    fn new_multiloop(header: Vec<&str>, body: Option<Vec<&str>>) -> Box<dyn Multiloop> {
        Box::new(USACS::new(header))
    }
}
impl Multiloop for USACS {
    fn get_type(&self) -> PacketType {
        PacketType::USACS
    }
    fn as_any(&self) -> &dyn Any {
        self
    }
    fn get_record_type(&self) -> &RecordType {
        &self.record_type
    }
}
// Some sort of DriverID packet but unsure exactly
// what the fields are.
// The packet has no body.
#[derive(Debug)]
pub struct USACDriverID {
    pub record_type: RecordType,
    pub uid: String,       // Unsure what this refers to. Car #?
    pub current_time: u64, // Milliseconds after the epoch. Best guess.
    // Unsure what this field could be. Driver # for switches as
    // it's regularly single digits? Occasional entry w/ 16240926
    pub unknown1: String,
}
impl USACDriverID {
    fn new(header: Vec<&str>) -> USACDriverID {
        USACDriverID {
            record_type: parse_result_type(header[1]).unwrap(),
            uid: header[2].to_string(), // u8::from_str_radix(header[2], 16).unwrap(),
            current_time: u64::from_str_radix(header[3], 16).unwrap(),
            unknown1: header[4].to_string(),
        }
    }
    fn new_multiloop(header: Vec<&str>, body: Option<Vec<&str>>) -> Box<dyn Multiloop> {
        Box::new(USACDriverID::new(header))
    }
}
impl Multiloop for USACDriverID {
    fn get_type(&self) -> PacketType {
        PacketType::USACDR
    }
    fn as_any(&self) -> &dyn Any {
        self
    }
    fn get_record_type(&self) -> &RecordType {
        &self.record_type
    }
}
/***************************
   CODEC

Notes:
Advanced real time data via the Multiloop protocol, supported by most strategy software:
Hostname: timing.usacnation.com
Port: 50003

Header ends with ¦¦
***************************
*/
#[derive(Debug, Clone)]
pub struct DecodeError;
impl fmt::Display for DecodeError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "Error decoding multiloop packet")
    }
}

// Parses uniqueid fields that return 2x 16-bit numbers from
// a single 32-bit number. It is assumed that if the number
// provider is less than 4 digits, the left side is 0 and the
// right side is the number in the packet
pub fn parse_variable_uniqueid(header: &str) -> (u16, u16) {
    if header.len() > 4 {
        (
            u16::from_str_radix(header[0..header.len() - 4].as_ref(), 16).unwrap(),
            u16::from_str_radix(header[header.len() - 4..header.len()].as_ref(), 16).unwrap(),
        )
    } else {
        (0, u16::from_str_radix(header, 16).unwrap())
    }
}

pub fn parse_result_type(result_type: &str) -> Result<RecordType, DecodeError> {
    match result_type {
        "N" => Ok(RecordType::N),
        "R" => Ok(RecordType::R),
        "U" => Ok(RecordType::U),
        _ => Err(DecodeError), // Record Type not defined by the standard
    }
}

///
/// decode_packet follows a return-early-on-error pattern
pub fn decode_packet(packet: &str) -> Result<Box<dyn Multiloop>, DecodeError> {
    let header_and_body = if (packet.find("USAC:").is_some()) {
        // USAC packets don't have bodies so any '¦¦' is
        // an empty field.
        vec![packet, ""]
    } else {
        // Remove \r\n from the end of the string
        // Header is 3 fields terminated in a ¦¦. Header fields separated by ¦
        // Assumption: No header field is empty
        packet.splitn(2, "¦¦").collect()
    };

    // Parse the header
    let header: Vec<&str> = header_and_body[0].split("¦").collect();

    // Parse the body if it exists
    let body: Option<Vec<&str>> = if header_and_body.len() > 1 {
        Some(header_and_body[1].split("¦").collect())
    } else {
        None
    };

    match header[0].strip_prefix('$').unwrap() {
        "H" => Ok(Heartbeat::new_multiloop(header, body.unwrap())),
        "E" => Ok(EntryInformation::new_multiloop(header, body.unwrap())),
        "C" => Ok(CompletedLap::new_multiloop(header, body.unwrap())),
        "S" => Ok(CompletedSection::new_multiloop(header, body.unwrap())),
        "L" => Ok(LineCrossing::new_multiloop(header, body.unwrap())),
        "I" => Ok(InvalidatedLap::new_multiloop(header, body.unwrap())),
        "F" => Ok(FlagInformation::new_multiloop(header, body.unwrap())),
        "N" => Ok(NewLeaderInformation::new_multiloop(header, body.unwrap())),
        "R" => Ok(RunInformation::new_multiloop(header, body.unwrap())),
        "T" => Ok(TrackInformation::new_multiloop(header, body.unwrap())),
        "A" => Ok(Announcement::new_multiloop(header, body.unwrap())),
        "V" => Ok(Version::new_multiloop(header, body.unwrap())),
        "USAC:TZ" => Ok(USACTimeZone::new_multiloop(header, body)),
        "USAC:S" => Ok(USACS::new_multiloop(header, body)),
        "USAC:DRIVERID" => Ok(USACDriverID::new_multiloop(header, body)),
        _ => Err(DecodeError),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn decode_heartbeat() {
        let hb = decode_packet("$H¦N¦1A¦¦U¦622DAF09¦0¦0¦200B20").unwrap();
        let hb = hb.as_any().downcast_ref::<Heartbeat>().unwrap();
        assert_eq!(hb.get_type(), PacketType::H);
        assert_eq!(hb.record_type, RecordType::N);
        assert_eq!(hb.heartbeats_sent, 26);
        assert_eq!(hb.track_status, TrackStatus::U);
        assert_eq!(hb.time_date, 1647161097);
        assert_eq!(hb.elapsed_time, 0);
        assert_eq!(hb.laps_to_go, 0);
        assert_eq!(hb.time_to_go, 2100000);
    }

    #[test]
    fn decode_completed_lap() {
        let cl =
            decode_packet("$C¦R¦B0000¦¦C¦54¦B¦0¦0¦0¦P¦0¦0¦0¦0¦0¦0¦C¦0¦Unknown¦ ¦0¦0¦C¦0").unwrap();
        let cl = cl.as_any().downcast_ref::<CompletedLap>().unwrap();
        assert_eq!(cl.get_type(), PacketType::C);
        assert_eq!(cl.record_type, RecordType::R);
        assert_eq!(cl.result_id, 11);
        assert_eq!(cl.lap_number, 0);
        assert_eq!(cl.rank, 12);
        assert_eq!(cl.number, "54");
        assert_eq!(cl.uid, 11);
        assert_eq!(cl.completed_laps, 0);
        assert_eq!(cl.elapsed_time, 0);
        assert_eq!(cl.last_laptime, 0);
        assert_eq!(cl.lap_status, LapStatus::P);
        assert_eq!(cl.fastest_laptime, 0);
        assert_eq!(cl.fastest_lap, 0);
        assert_eq!(cl.time_behind_leader, 0);
        assert_eq!(cl.laps_behind_leader, 0);
        assert_eq!(cl.time_behind_prec, 0);
        assert_eq!(cl.laps_behind_prec, 0);
        assert_eq!(cl.overall_rank, 12);
        assert_eq!(cl.overall_best_laptime, 0);
        assert_eq!(cl.current_status, "Unknown");
        assert_eq!(cl.track_status, None);
        assert_eq!(cl.pit_stop_count, 0);
        assert_eq!(cl.last_pitted_lap, 0);
        assert_eq!(cl.start_position, 12);
        assert_eq!(cl.laps_led, 0);
    }

    #[test]
    fn decode_completed_section() {
        let cs = decode_packet("$S¦N¦85E60000¦¦65¦85E6¦S1¦1EB1FC0¦9E4F¦0").unwrap();
        let cs = cs.as_any().downcast_ref::<CompletedSection>().unwrap();
        assert_eq!(cs.get_type(), PacketType::S);
        assert_eq!(cs.record_type, RecordType::N);
        assert_eq!(cs.result_id, 34278);
        assert_eq!(cs.sector_id, 0);
        assert_eq!(cs.number, "65");
        assert_eq!(cs.uid, 34278);
        assert_eq!(cs.section_name, "S1");
        assert_eq!(cs.elapsed_time, 32186304);
        assert_eq!(cs.last_section_time, 40527);
        assert_eq!(cs.last_lap, 0);

        // Real world example from 20230429 at NOLA. Does not follow published spec.
        let cs = decode_packet("$S¦N¦95590000¦¦101¦79559¦S1¦2C6B8B7¦980F¦0").unwrap();
        let cs = cs.as_any().downcast_ref::<CompletedSection>().unwrap();
        assert_eq!(cs.get_type(), PacketType::S);
        assert_eq!(cs.record_type, RecordType::N);
        assert_eq!(cs.result_id, 38233);
        assert_eq!(cs.sector_id, 0);
        assert_eq!(cs.number, "101");
        assert_eq!(cs.uid, 38233); // Unsure what the '7' is fo in front of the 9559
        assert_eq!(cs.section_name, "S1");
        assert_eq!(cs.elapsed_time, 46577847);
        assert_eq!(cs.last_section_time, 38927);
        assert_eq!(cs.last_lap, 0);
    }

    #[test]
    fn decode_line_crossing() {
        let lc = decode_packet("$L¦N¦860C¦¦87¦4¦IM1¦A¦B2C5¦G¦T").unwrap();
        let lc = lc.as_any().downcast_ref::<LineCrossing>().unwrap();
        assert_eq!(lc.get_type(), PacketType::L);
        assert_eq!(lc.record_type, RecordType::N);
        assert_eq!(lc.passing_id, 34316);
        assert_eq!(lc.number, "87");
        assert_eq!(lc.uid, 4);
        assert_eq!(lc.time_line, "IM1");
        assert_eq!(lc.source, LineCrossingSource::A);
        assert_eq!(lc.elapsed_time, 45765);
        assert_eq!(lc.track_status, TrackStatus::G);
        assert_eq!(lc.crossing_status, LapStatus::T);
    }

    #[test]
    fn decode_invalidated_lap() {
        let il = decode_packet("$I¦R¦7A180¦¦07¦7A180¦281C99¦").unwrap();
        let il = il.as_any().downcast_ref::<InvalidatedLap>().unwrap();
        assert_eq!(il.get_type(), PacketType::I);
        assert_eq!(il.record_type, RecordType::R);
        assert_eq!(il.passing_id, 500096);
        assert_eq!(il.number, "07");
        assert_eq!(il.uid, 500096);
        assert_eq!(il.elapsed_time, 2628761)
    }

    #[test]
    fn decode_flag_information() {
        let fi = decode_packet("$F¦R¦2¦¦G¦0¦2EF¦0¦0¦0¦0¦0¦22¦0¦0").unwrap();
        let fi = fi.as_any().downcast_ref::<FlagInformation>().unwrap();
        assert_eq!(fi.get_type(), PacketType::F);
        assert_eq!(fi.record_type, RecordType::R);
        assert_eq!(fi.flag_changes, 2);
        assert_eq!(fi.track_status, TrackStatus::G);
        assert_eq!(fi.lap_number, 0);
        assert_eq!(fi.green_time, 751);
        assert_eq!(fi.green_laps, 0);
        assert_eq!(fi.yellow_time, 0);
        assert_eq!(fi.yellow_laps, 0);
        assert_eq!(fi.red_time, 0);
        assert_eq!(fi.num_yellows, 0);
        assert_eq!(fi.current_leader, "22");
        assert_eq!(fi.num_lead_changes, 0);
        assert_eq!(fi.avg_race_speed, "0");
    }

    #[test]
    fn decode_new_leader_information() {
        let nl = decode_packet("$N¦N¦1¦¦22¦0¦1¦224F9¦1").unwrap();
        let nl = nl.as_any().downcast_ref::<NewLeaderInformation>().unwrap();
        assert_eq!(nl.get_type(), PacketType::N);
        assert_eq!(nl.record_type, RecordType::N);
        assert_eq!(nl.num_lead_changes, 1);
        assert_eq!(nl.new_leader, "22");
        assert_eq!(nl.uid, 0);
        assert_eq!(nl.lap_number, 1);
        assert_eq!(nl.elapsed_time, 140537);
        assert_eq!(nl.lead_change_index, 1);
    }

    #[test]
    fn decode_entry_information() {
        let ei = decode_packet(
            "$E¦R¦4¦¦23¦4¦Christian Weir¦5¦B¦Cayman¦¦MDK Motorsports¦¦Dublin, OH¦¦¦¦¦¦¦4¦",
        )
        .unwrap();
        let ei = ei.as_any().downcast_ref::<EntryInformation>().unwrap();
        assert_eq!(ei.get_type(), PacketType::E);
        assert_eq!(ei.record_type, RecordType::R);
        assert_eq!(ei.result_id, 4);
        assert_eq!(ei.number, "23");
        assert_eq!(ei.uid, 4);
        assert_eq!(ei.driver_name, "Christian Weir");
        assert_eq!(ei.start_pos, 5);
        assert_eq!(ei.info_count, 11);
        assert_eq!(
            ei.info,
            vec![
                "Cayman",
                "",
                "MDK Motorsports",
                "",
                "Dublin, OH",
                "",
                "",
                "",
                "",
                "",
                ""
            ]
        );
        assert_eq!(ei.competitor_id, 4);
    }

    #[test]
    fn decode_run_information() {
        let ri = decode_packet(
            "$R¦R¦4000002D¦¦Porsche Sprint Challenge - Sebring¦¦Cayman Race 1¦R¦622DDCC0",
        )
        .unwrap();
        let ri = ri.as_any().downcast_ref::<RunInformation>().unwrap();
        assert_eq!(ri.get_type(), PacketType::R);
        assert_eq!(ri.record_type, RecordType::R);
        assert_eq!(ri.run_id, 1073741869);
        assert_eq!(ri.event_name, "Porsche Sprint Challenge - Sebring");
        assert_eq!(ri.short_name, "");
        assert_eq!(ri.run_name, "Cayman Race 1");
        assert_eq!(ri.run_type, RunType::R);
        assert_eq!(ri.start_datetime, 1647172800);
    }

    #[test]
    fn decode_track_information() {
        let ti = decode_packet("$T¦R¦3¦¦Sebring International Raceway¦¦3.74¦3¦S1¦71818¦SF¦IM1¦S2¦73370¦IM1¦IM2¦S3¦89267¦IM2¦SF").unwrap();
        let ti = ti.as_any().downcast_ref::<TrackInformation>().unwrap();
        assert_eq!(ti.get_type(), PacketType::T);
        assert_eq!(ti.record_type, RecordType::R);
        assert_eq!(ti.track_id, 3);
        assert_eq!(ti.track_name, "Sebring International Raceway");
        assert_eq!(ti.venue, "");
        assert_eq!(ti.track_length, "3.74");
        assert_eq!(ti.num_sections, 3);
        assert_eq!(
            ti.section_info,
            vec![
                SectionInformation {
                    section_name: "S1".to_string(),
                    section_length: "71818".to_string(),
                    section_start_lbl: "SF".to_string(),
                    section_end_lbl: "IM1".to_string(),
                },
                SectionInformation {
                    section_name: "S2".to_string(),
                    section_length: "73370".to_string(),
                    section_start_lbl: "IM1".to_string(),
                    section_end_lbl: "IM2".to_string(),
                },
                SectionInformation {
                    section_name: "S3".to_string(),
                    section_length: "89267".to_string(),
                    section_start_lbl: "IM2".to_string(),
                    section_end_lbl: "SF".to_string(),
                }
            ]
        );
    }

    #[test]
    #[ignore = "not yet implemented -- no ground truth test data"]
    fn decode_announcement() {
        todo!()
    }

    #[test]
    fn decode_version() {
        let v =
            decode_packet("$V¦R¦1¦¦1¦5¦Multiloop feed 5.6.2 Copyright by United States Auto Club")
                .unwrap();
        let v: &Version = v.as_any().downcast_ref::<Version>().unwrap();
        assert_eq!(v.get_type(), PacketType::V);
        assert_eq!(v.record_type, RecordType::R);
        assert_eq!(v.major, 1);
        assert_eq!(v.minor, 5);
        assert_eq!(
            v.info,
            "Multiloop feed 5.6.2 Copyright by United States Auto Club"
        );
    }

    #[test]
    fn decode_version_abnormal() {
        let v = decode_packet("$V¦N¦1¦¦").unwrap();
        let v: &Version = v.as_any().downcast_ref::<Version>().unwrap();
        assert_eq!(v.get_type(), PacketType::V);
        assert_eq!(v.record_type, RecordType::N);
        assert_eq!(v.major, 0);
        assert_eq!(v.minor, 0);
        assert_eq!(v.info, "");
    }

    #[test]
    fn decode_usactz() {
        let tz = decode_packet("$USAC:TZ¦U¦America/Chicago¦187CF74EDB7").unwrap();
        let tz: &USACTimeZone = tz.as_any().downcast_ref().unwrap();
        assert_eq!(tz.get_type(), PacketType::USACTZ);
        assert_eq!(tz.record_type, RecordType::U);
        assert_eq!(tz.tz, "America/Chicago");
        assert_eq!(tz.current_time, 1682812759479)
    }

    #[test]
    fn decode_usacs() {
        let s = decode_packet("$USAC:S¦N¦8¦187CF746135¦VMAX¦1F410").unwrap();
        let s: &USACS = s.as_any().downcast_ref().unwrap();
        assert_eq!(s.get_type(), PacketType::USACS);
        assert_eq!(s.record_type, RecordType::N);
        assert_eq!(s.uid, "8");
        assert_eq!(s.current_time, 1682812723509);
        assert_eq!(s.sector_name, "VMAX");
        assert_eq!(s.unknown1, 128016);
    }

    #[test]
    fn decode_usacdriverid() {
        let driverid = decode_packet("$USAC:DRIVERID¦U¦¦187CE221F63¦3").unwrap();
        let driverid: &USACDriverID = driverid.as_any().downcast_ref().unwrap();
        assert_eq!(driverid.get_type(), PacketType::USACDR);
        assert_eq!(driverid.record_type, RecordType::U);
        assert_eq!(driverid.uid, "");
        assert_eq!(driverid.current_time, 1682790555491);
        assert_eq!(driverid.unknown1, "3")
    }
}
